package com.api.spring.entity;

/**
 * Created by User on 7/20/2020.
 */
public enum OrdersStatus {

    PACKAGING,SHIPPING,DELIVERED
}
