package com.api.spring.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * Created by User on 6/25/2020.
 */

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="address_table")
public class Address {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="address_type")
    private String addressType;

    @Column(name="house")
    private String house;

    @Column(name="city")
    private String city;

    @Column(name="zip")
    private Integer zip;


}
