package com.api.spring.entity;

import com.api.spring.rest.dto.CartDto;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
public class Cart extends BaseEntity{

    @OneToMany(cascade = CascadeType.ALL)
    private List<CartItem> cartItems;

    @OneToOne(cascade = CascadeType.ALL)
    private User user;

    private BigDecimal totalCartPrice;
}
