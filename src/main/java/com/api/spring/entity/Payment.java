package com.api.spring.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Data
@Entity
public class Payment extends BaseEntity{

    @OneToOne
    private Orders orders;


}
