package com.api.spring.entity;

import com.fasterxml.jackson.databind.ser.Serializers;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
public class CartItem extends BaseEntity{

    @OneToOne(cascade = CascadeType.ALL)
    private Product product;

    private Integer quantity;

    private BigDecimal itemPrice;
}
