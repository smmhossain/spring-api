package com.api.spring.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="user_table")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id" , nullable = false)
    private Integer id;

    @Column(name="name" , nullable = false)
    private String name;

    @Column(name="age" , nullable = false)
    private Integer age;

    @Column(name="sex" ,nullable = false)
    private String sex;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id" , referencedColumnName = "id" ,nullable = false)
    private Address address;


}
