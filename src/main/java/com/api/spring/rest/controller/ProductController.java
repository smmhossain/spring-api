package com.api.spring.rest.controller;

import com.api.spring.entity.Product;
import com.api.spring.rest.dto.ProductDto;
import com.api.spring.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ProductController.ROOT_URL)
public class ProductController {

    public static final String ROOT_URL = "/api/products";

    private final ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductDto>> getAllProducts(){

        log.debug("find all products ");
        return ResponseEntity.ok(
                productService.getAllProducts()
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable String id){

        log.debug("find product by id : "+id);
        return ResponseEntity.ok(
                productService.getProductById(id)
        );
    }


    @PostMapping
    public ResponseEntity<ProductDto> create(@RequestBody @Validated ProductDto productDto){

        log.debug("insert new object  : "+productDto.toString());
        return ResponseEntity.status(HttpStatus.CREATED).body(
                productService.createProduct(productDto)
        );
    }

    @PutMapping
    public ResponseEntity<ProductDto> update(@RequestBody @Validated ProductDto productDto){

        log.debug("update existing object : "+productDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(
                productService.updateProduct(productDto)
        );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable String id){

        log.debug("delete product with this id : "+id);
        productService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
