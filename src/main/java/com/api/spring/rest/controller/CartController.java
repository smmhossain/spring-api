package com.api.spring.rest.controller;

import com.api.spring.entity.Cart;
import com.api.spring.entity.User;
import com.api.spring.repository.CartRepository;
import com.api.spring.rest.dto.CartDto;
import com.api.spring.service.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(CartController.ROOT_URL)
public class CartController {

    public static final String ROOT_URL = "/api/cart/";
    private final CartService cartService;


    @GetMapping("/{userId}")
    public ResponseEntity<Cart> getCart(@PathVariable int userId){

        log.debug("get Cart with user");
        return ResponseEntity.ok(
                cartService.getCartByUser(userId)
        );
    }

    @GetMapping("/c/{id}")
    public ResponseEntity<CartDto> Cart(@PathVariable String id){

        return ResponseEntity.ok(
                cartService.getById(id)
        );
    }
}
