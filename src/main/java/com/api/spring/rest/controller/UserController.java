package com.api.spring.rest.controller;

import com.api.spring.entity.User;
import com.api.spring.rest.dto.UserDto;
import com.api.spring.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(UserController.ROOT_URL)
public class UserController {

    public static final String ROOT_URL = "/api/users";
    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers(){

        return ResponseEntity.ok(
                userService.getAllUsers()
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer id) {

        log.debug("find by {id}",id);
        return ResponseEntity.ok(
                userService.getById(id)
        );
    }

    @PostMapping
    public ResponseEntity<UserDto> createUser(@RequestBody @Validated UserDto userDto){

        log.debug("creating object : {}",userDto.toString());
        return ResponseEntity.status(HttpStatus.CREATED).body(
                userService.create(userDto)
        );
    }

    @PutMapping
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto) {

        log.debug("updating object: {}",userDto.toString());
        return ResponseEntity.status(HttpStatus.OK).body(
                userService.update(userDto)
        );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer id){

        log.debug("delete by id : ",id);
        userService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
