package com.api.spring.rest.controller;

import com.api.spring.entity.Orders;
import com.api.spring.entity.User;
import com.api.spring.rest.dto.OrdersDto;
import com.api.spring.service.OrdersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(OrdersController.ROOT_URL)
public class OrdersController {

    public final static String ROOT_URL = "api/orders";

    private final OrdersService orderService;

    @PostMapping
    public ResponseEntity<OrdersDto> create(@RequestBody @Validated User user, OrdersDto ordersDto){

        log.debug("create object : "+ ordersDto.toString());
        return ResponseEntity.status(HttpStatus.CREATED).body(
                orderService.createOrder(user, ordersDto)
        );
    }

    @GetMapping("users/{userId}")
    public ResponseEntity<Orders> getOrderByUser(@PathVariable Integer userId){

        log.debug("get order with user id : "+userId);
        return ResponseEntity.ok(orderService.getOrderByUser(userId));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrdersDto> getOrderById(@PathVariable String id){

        log.debug("find order by id : "+id);
        return ResponseEntity.ok(orderService.getOrderById(id));
    }


}
