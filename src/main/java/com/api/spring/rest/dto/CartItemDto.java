package com.api.spring.rest.dto;

import com.api.spring.entity.CartItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartItemDto {


    private String id;

    private ProductDto product;

    private Integer quantity;

    private BigDecimal itemPrice;


}
