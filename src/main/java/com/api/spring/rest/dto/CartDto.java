package com.api.spring.rest.dto;


import com.api.spring.entity.Cart;
import com.api.spring.entity.CartItem;
import com.api.spring.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartDto {

    private String id;

    private List<CartItemDto> cartItems;

    private UserDto user;

    private BigDecimal totalCartPrice;
}
