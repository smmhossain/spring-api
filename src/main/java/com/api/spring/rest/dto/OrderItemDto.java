package com.api.spring.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto {

    private ProductDto product;

    private Integer quantity;

    private String orderItemStatus;

    private BigDecimal itemPrice;
}
