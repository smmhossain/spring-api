package com.api.spring.rest.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrdersDto {


    private List<CartItemDto> orderItems;

    private AddressDto billingAddress;

    private AddressDto shippingAddress;

    private UserDto user;

    private BigDecimal totalPrice;

    private String orderStatus;
}
