package com.api.spring.rest.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {


    private Integer id;

    @NotNull(message = "name can not be not null")
    private String name;

    @NotNull(message = "age can not be not null")
    @Min(value = 1, message = "age can not be less than 1")
    private Integer age;

    @NotNull(message = "sex can not be null and sex must")
    private String sex;

    @NotNull(message = "address can not be null")
    private AddressDto address;


}

