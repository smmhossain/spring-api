package com.api.spring.rest.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {


    private Integer id;

    @NotNull(message = "addressType can not be null and addressType must be HOME or OFFICE")
    private String addressType;

    @NotNull(message = "city can not be null")
    private String city;

    @NotNull(message = "house can not be null")
    private String house;

    @NotNull(message = "zip can not be null")
    @Min(value = 0 ,message = "zip can not be minus")
    private Integer zip;
}
