package com.api.spring.service.impl;

import com.api.spring.entity.Orders;
import com.api.spring.entity.User;
import com.api.spring.exception.OrdersNotFoundException;
import com.api.spring.repository.OrdersRepository;
import com.api.spring.rest.dto.OrdersDto;
import com.api.spring.service.OrderValidationService;
import com.api.spring.service.OrdersService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrdersServiceImpl implements OrdersService {

    private final OrdersRepository orderRepository;
    private final ModelMapper modelMapper;
    private final OrderValidationService orderValidationService;

    @Override
    public OrdersDto createOrder(User user, OrdersDto ordersDto) {
        orderValidationService.validateStatus(ordersDto.getStatus());
        orderValidationService.validateUser(user);
        Orders orders = modelMapper.map(ordersDto,Orders.class);
        orders.setUser(user);
        orderRepository.save(orders);
        return modelMapper.map(orders,OrdersDto.class);
    }

    @Override
    public Orders getOrderByUser(Integer userId) {
        return orderRepository.findOrderByUserId(userId);
    }

    @Override
    public OrdersDto getOrderById(String id) {
        return orderRepository.findById(id)
                .map(order -> modelMapper.map(order,OrdersDto.class))
                .orElseThrow(() -> new OrdersNotFoundException("Orders not found with this id : "+id)
                );
    }
}
