package com.api.spring.service.impl;


import com.api.spring.exception.UserNotFoundException;
import com.api.spring.repository.AddressRepository;
import com.api.spring.repository.UserRepository;
import com.api.spring.entity.Address;
import com.api.spring.entity.User;
import com.api.spring.rest.dto.AddressDto;
import com.api.spring.rest.dto.UserDto;
import com.api.spring.service.UserService;
import com.api.spring.service.ValidationService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final ValidationService validationService;
    private final ModelMapper modelMapper;

    @Override
    public List<UserDto> getAllUsers() {


        return ((List<User>) userRepository
                .findAll())
                .stream()
                .map(user -> modelMapper.map(user,UserDto.class))
                .collect(Collectors.toList());
    }


//    public List<UserLocationDTO> getAllUsersLocation() {
//        return ((List<User>) userRepository
//                .findAll())
//                .stream()
//                .map(this::convertToUserLocationDTO)
//                .collect(Collectors.toList());
//    }

    private UserDto convertToUserDTO(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setSex(user.getSex());
        userDto.setName(user.getName());
        userDto.setAge(user.getAge());
        Address address = user.getAddress();
        AddressDto addressDto =new AddressDto();
        addressDto.setZip(address.getZip());
        addressDto.setHouse(address.getHouse());
        addressDto.setCity(address.getCity());
        addressDto.setAddressType(address.getAddressType());
        addressDto.setId(address.getId());
        userDto.setAddress(addressDto);

        return userDto;
    }



    @Override
    public UserDto getById(Integer id) {

        return userRepository.findById(id)
                .map(user -> modelMapper.map(user,UserDto.class))
                .orElseThrow(()-> new UserNotFoundException("User not found with this id :"+id));
    }

    @Override
    public UserDto create(UserDto userDto) {

        AddressDto addressDto = userDto.getAddress();
        validationService.validateName(userDto.getName());
        validationService.validateAge(userDto.getAge());
        validationService.validateSex(userDto.getSex());
        validationService.validateAddressType(addressDto.getAddressType());
        validationService.validateHouse(addressDto.getHouse());
        validationService.validateCity(addressDto.getCity());
        validationService.validateZipCode(addressDto.getZip());

        User u = modelMapper.map(userDto,User.class);
        userRepository.save(u);
        return modelMapper.map(u, UserDto.class);
    }

    @Override
    public UserDto update(UserDto userDto) {

        User u = userRepository.findById(userDto.getId())
                .orElseThrow(() -> new UserNotFoundException(
                        "User not found to update by the id :"+userDto.getId()
                )
        );

        u.setSex(userDto.getSex());
        u.setAge(userDto.getAge());
        u.setName(userDto.getName());

        AddressDto addressDetails=userDto.getAddress();
        Integer adId = addressDetails.getId();
        Address address =  addressRepository.findById(adId).orElseThrow(()-> new UserNotFoundException("Address not found for this id : "+adId));
        address.setAddressType(addressDetails.getAddressType());
        address.setCity(addressDetails.getCity());
        address.setHouse(addressDetails.getHouse());
        address.setZip(addressDetails.getZip());

        u.setAddress(address);

        return Optional.of(userRepository.save(u))
                .map(user1 -> modelMapper.map(u,UserDto.class))
                .orElseThrow(() -> new UserNotFoundException("User update fails."));

    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

}
