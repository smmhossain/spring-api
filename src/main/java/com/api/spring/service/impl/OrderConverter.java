package com.api.spring.service.impl;

import com.api.spring.entity.Address;
import com.api.spring.entity.Orders;
import com.api.spring.rest.dto.AddressDto;
import com.api.spring.rest.dto.OrdersDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;

@RequiredArgsConstructor
public class OrderConverter {

    private final ModelMapper modelMapper;

    public Orders fromOrderDtoToOrder(OrdersDto ordersDto){
        Orders orders = new Orders();
        orders.setStatus(ordersDto.getStatus());

        AddressDto addressDto = ordersDto.getBillingAddress();
        Address address = new Address();
        address.setZip(addressDto.getZip());
        address.setHouse(addressDto.getHouse());
        address.setCity(addressDto.getCity());
        address.setAddressType(addressDto.getAddressType());
        address.setId(addressDto.getId());
        orders.setBillingAddress(address);


        return orders;

    }

    public OrdersDto fromOrderToOrderDto(Orders orders){
        return OrdersDto.builder()
                .status(orders.getStatus())
                .build();
    }
}
