package com.api.spring.service.impl;

import com.api.spring.entity.Product;
import com.api.spring.exception.CartNotFoundException;
import com.api.spring.exception.ProductNotFoundException;
import com.api.spring.repository.ProductRepository;
import com.api.spring.rest.dto.ProductDto;
import com.api.spring.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<ProductDto> getAllProducts() {

        return ((List<Product>) productRepository
                .findAll())
                .stream()
                .map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto createProduct(ProductDto productDto) {

        Product p =modelMapper.map(productDto,Product.class);
        productRepository.save(p);
        return modelMapper.map(p, ProductDto.class);
    }

    @Override
    public ProductDto updateProduct(ProductDto productDto) {

        Product p = productRepository.findById(productDto.getId())
                .orElseThrow(() -> new ProductNotFoundException(
                        "Product not found with this id : "+productDto.getId())
         );

        p.setPrice(productDto.getPrice());

        return Optional.of(productRepository.save(p))
                .map(product -> modelMapper.map(p,ProductDto.class))
                .orElseThrow(() -> new ProductNotFoundException("product update fails."));
    }

    @Override
    public ProductDto getProductById(String id) {
        return productRepository.findById(id)
                .map(product -> modelMapper.map(product,ProductDto.class))
                .orElseThrow(() -> new ProductNotFoundException("Product Not found with this id : "+id));
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
    }
}
