package com.api.spring.service.impl;


import com.api.spring.entity.Cart;
import com.api.spring.exception.CartNotFoundException;
import com.api.spring.repository.CartRepository;
import com.api.spring.rest.dto.CartDto;
import com.api.spring.service.CartService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;
    private final ModelMapper modelMapper;

    @Override
    public Cart getCartByUser(Integer userId) {

        return cartRepository
                .findCartByUserId(userId);
    }

    @Override
    public CartDto getById(String id) {
        return cartRepository.findById(id)
                .map(cart -> modelMapper.map(cart,CartDto.class))
                .orElseThrow(()-> new CartNotFoundException("Cart not found with this id : "+id));
    }


}
