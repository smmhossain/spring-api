package com.api.spring.service.impl;

import com.api.spring.entity.Address;
import com.api.spring.entity.OrdersStatus;
import com.api.spring.entity.User;
import com.api.spring.exception.InvalidArgumentException;
import com.api.spring.service.OrderValidationService;
import org.springframework.stereotype.Service;

@Service
public class OrderValidationServiceImpl implements OrderValidationService {


    @Override
    public void validateStatus(String status) {
        status = status.toUpperCase();
        if(status==null){
            throw new InvalidArgumentException("status cannot be null");
        }else if(!status.equals(OrdersStatus.DELIVERED) && !status.equals(OrdersStatus.PACKAGING) && !status.equals(OrdersStatus.SHIPPING)){
            throw new InvalidArgumentException("status should be PACKAGING or DELIVERED or SHIPPING");
        }
    }

    @Override
    public void validateBillingAddress(Address billingAddress) {
        if(billingAddress==null){
            throw new InvalidArgumentException("BilingAddress can not be null");
        }

    }

    @Override
    public void validateShippingAddress(Address shippingAddress) {

        if(shippingAddress==null){
            throw new InvalidArgumentException("Shipping address can not be null");
        }
    }

    @Override
    public void validateUser(User user) {

        if(user==null){
            throw new InvalidArgumentException("User can not be null");
        }
    }
}
