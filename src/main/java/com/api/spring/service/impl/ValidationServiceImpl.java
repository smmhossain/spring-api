package com.api.spring.service.impl;

import com.api.spring.entity.AddressType;
import com.api.spring.exception.InvalidArgumentException;
import com.api.spring.service.ValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.valves.rewrite.InternalRewriteMap;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@Component
@RequiredArgsConstructor
public class ValidationServiceImpl implements ValidationService {

    private static final int MIN_ZIP = 0;
    private static final int MIN_AGE = 1;
    private static final int MAX_AGE = 150;
    private static final String SEX_MALE = "MALE";
    private static final String SEX_FEMALE = "FEMALE";
    private static final String SEX_CUSTOM = "CUSTOM";


    @Override
    public void validateSex(String sex) {

        log.debug("validating user sex");
        sex = sex.toUpperCase();
        if((!sex.equals(SEX_MALE)) && (!sex.equals(SEX_FEMALE)) && (!sex.equals(SEX_CUSTOM))){
            throw new InvalidArgumentException("User Sex is invalid.You must use Male, Female or Custom");
        }
    }

    @Override
    public void validateName(String name) {

        log.debug("validating name");
        int nameStrLen = name.length();

        if (nameStrLen ==0) {
            throw new InvalidArgumentException("name can not be null");
        }
        else if(nameStrLen ==1) {
            throw new InvalidArgumentException("invalid name");
        }
        else{
            for (int i = 0; i < nameStrLen; i++) {

                if (!((name.charAt(i) >= 'a' && name.charAt(i) <= 'z')
                        || (name.charAt(i) >= 'A' && name.charAt(i) <= 'Z'))) {
                    throw new InvalidArgumentException("name must be start with letter");
                }
            }
        }
    }

    @Override
    public void validateAge(int age) {

        log.debug("validating age");
        if(age < MIN_AGE){
            throw new InvalidArgumentException("age can not be less than 1");
        }else if(age > MAX_AGE){
            throw new InvalidArgumentException("age can not be greater than 150");
        }
    }

    @Override
    public void validateZipCode(int zip) {
        log.debug("validating zip code");
        if (zip < MIN_ZIP){
            throw new InvalidArgumentException("zip code can not be minus");
        }
    }

    @Override
    public void validateAddressType(String addressType) {

        log.debug("validating addressType");
        if(!addressType.equals(AddressType.HOME) && !addressType.equals(AddressType.OFFICE)){
            throw new InvalidArgumentException("address type must be HOME or OFFICE");
        }
    }

    @Override
    public void validateCity(String city) {

        log.debug("validating city");
        int cityStrLen = city.length();
        for(int i = 0 ; i < cityStrLen ; i++){
            if(!((city.charAt(i) >= 'a' && city.charAt(i) <= 'z')
                || (city.charAt(i)>= 'A' && city.charAt(i) <= 'Z'))) {
            throw new InvalidArgumentException("city must be start with letter");
            }
        }
    }

    @Override
    public void validateHouse(String house) {

        log.debug("validating house");
        if(!((house.charAt(0) >= 'a' && house.charAt(0) <= 'z')
                || (house.charAt(0)>= 'A' && house.charAt(0) <= 'Z')
                || (house.charAt(0)>= '0' && house.charAt(0)<='9'))){
            throw new InvalidArgumentException("house can not be start with special character");
        }
    }

}
