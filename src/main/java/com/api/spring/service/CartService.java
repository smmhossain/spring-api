package com.api.spring.service;


import com.api.spring.entity.Cart;
import com.api.spring.rest.dto.CartDto;
import org.springframework.data.domain.Page;

public interface CartService {

    Cart getCartByUser(Integer userId);
    CartDto getById(String id);
}
