package com.api.spring.service;

import com.api.spring.entity.Address;
import com.api.spring.entity.User;

/**
 * Created by User on 7/20/2020.
 */
public interface OrderValidationService {

    void validateStatus(String status);
    void validateBillingAddress(Address billingAddress);
    void validateShippingAddress(Address shippingAddress);
    void validateUser(User user);
}
