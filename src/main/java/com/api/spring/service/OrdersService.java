package com.api.spring.service;


import com.api.spring.entity.Orders;
import com.api.spring.entity.User;
import com.api.spring.rest.dto.OrdersDto;

public interface OrdersService {

    OrdersDto createOrder(User user, OrdersDto ordersDto);
    Orders getOrderByUser(Integer userId);
    OrdersDto getOrderById(String id);

}
