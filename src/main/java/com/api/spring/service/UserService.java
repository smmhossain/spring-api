package com.api.spring.service;

import com.api.spring.entity.User;
import com.api.spring.rest.dto.UserDto;

import java.util.List;


public interface UserService {

    List<UserDto> getAllUsers();
    UserDto getById(Integer id);
    UserDto create(UserDto userDto);
    UserDto update(UserDto userDto);
    void delete(Integer id);

}
