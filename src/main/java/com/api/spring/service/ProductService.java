package com.api.spring.service;


import com.api.spring.entity.Product;
import com.api.spring.rest.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAllProducts();
    ProductDto createProduct(ProductDto productDto);
    ProductDto updateProduct(ProductDto productDto);
    ProductDto getProductById(String id);
    void delete(String id);
}
