package com.api.spring.service;

import java.math.BigDecimal;


public interface ValidationService {

    void validateSex(String sex);
    void validateName(String name);
    void validateAge(int age);

    void validateZipCode(int zip);
    void validateAddressType(String addressType);
    void validateCity(String city);
    void validateHouse(String house);
}
