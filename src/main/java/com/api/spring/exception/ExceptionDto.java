package com.api.spring.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.List;


@Builder
@Getter
@AllArgsConstructor
public class ExceptionDto {

    private final Date timestamp;

    private final List<String> messages;

    private final String details;



}
