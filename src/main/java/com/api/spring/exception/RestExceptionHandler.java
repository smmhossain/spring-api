package com.api.spring.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;


import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler extends RuntimeException{

    @ExceptionHandler(InvalidArgumentException.class)
    public final ResponseEntity<ExceptionDto> notFoundException(InvalidArgumentException ex, WebRequest request){

        log.debug("notFoundException method visited");
        ExceptionDto exceptionDto = ExceptionDto.builder()
                .timestamp(new Date())
                .messages(Collections.singletonList(ex.getMessage()))
                .details(request.getDescription(false))
                .build();
        return  new ResponseEntity<>(exceptionDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<ExceptionDto> userNotFoundException(UserNotFoundException ex,WebRequest request){

        log.debug("user not found exception handler visited");
        ExceptionDto exceptionDto = ExceptionDto.builder()
                .timestamp(new Date())
                .messages(Collections.singletonList(ex.getMessage()))
                .details(request.getDescription(false))
                .build();
        return new ResponseEntity<>(exceptionDto,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ExceptionDto> handleConstrainViolationException(MethodArgumentNotValidException ex, WebRequest request){

        ExceptionDto exceptionDto = ExceptionDto.builder()
                .timestamp(new Date())
                .messages(ex.getBindingResult().getFieldErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(Collectors.toList())
                )
                .details(request.getDescription(false))
                .build();
        return new ResponseEntity<>(exceptionDto,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CartNotFoundException.class)
    public final ResponseEntity<ExceptionDto> handleCartNotFoundException(CartNotFoundException ex,WebRequest request){

        log.debug("CartNotFoundException handle");
        ExceptionDto exceptionDto = ExceptionDto.builder()
                .timestamp(new Date())
                .messages(Collections.singletonList(ex.getMessage()))
                .details(request.getDescription(false))
                .build();
        return new ResponseEntity<>(exceptionDto,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public final ResponseEntity<ExceptionDto> productNotFoundExceptionHandle(ProductNotFoundException ex,WebRequest request){

        log.debug("ProductNotFoundException handle");
        ExceptionDto exceptionDto = ExceptionDto.builder()
                .timestamp(new Date())
                .messages(Collections.singletonList(ex.getMessage()))
                .details(request.getDescription(false))
                .build();
        return new ResponseEntity<>(exceptionDto,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(OrdersNotFoundException.class)
    public final ResponseEntity<ExceptionDto> orderNotFoundExceptionHandler(OrdersNotFoundException ex, WebRequest request){

        log.debug("OrdersNotFoundException handling");
        ExceptionDto exceptionDto = ExceptionDto.builder()
                .timestamp(new Date())
                .messages(Collections.singletonList(ex.getMessage()))
                .details(request.getDescription(false))
                .build();
        return new ResponseEntity<>(exceptionDto,HttpStatus.NOT_FOUND);
    }
}
