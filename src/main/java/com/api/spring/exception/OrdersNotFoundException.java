package com.api.spring.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@RequiredArgsConstructor
public class OrdersNotFoundException extends RuntimeException {

    private final String message;
}
