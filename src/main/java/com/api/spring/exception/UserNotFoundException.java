package com.api.spring.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by User on 6/27/2020.
 */
@Setter
@Getter
@AllArgsConstructor
public class UserNotFoundException extends RuntimeException {

    private final String message;
}
