package com.api.spring.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ProductNotFoundException extends RuntimeException {

    private final String message;
}
