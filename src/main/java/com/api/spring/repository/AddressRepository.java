package com.api.spring.repository;

import com.api.spring.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by User on 6/25/2020.
 */
@Repository
public interface AddressRepository extends JpaRepository<Address,Integer> {
}
