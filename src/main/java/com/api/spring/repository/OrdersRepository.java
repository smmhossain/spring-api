package com.api.spring.repository;

import com.api.spring.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, String> {

    Orders findOrderByUserId(Integer userId);
}
