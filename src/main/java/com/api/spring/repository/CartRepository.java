package com.api.spring.repository;

import com.api.spring.entity.Cart;
import com.api.spring.entity.User;
import com.api.spring.rest.dto.CartDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, String> {

    Cart findCartByUserId(Integer userId);

}
