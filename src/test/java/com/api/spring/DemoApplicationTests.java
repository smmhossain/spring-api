package com.api.spring;

import com.api.spring.entity.Address;
import com.api.spring.entity.Product;
import com.api.spring.entity.User;
import com.api.spring.repository.UserRepository;
import com.api.spring.rest.dto.AddressDto;
import com.api.spring.rest.dto.UserDto;
import com.api.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class DemoApplicationTests {

	@MockBean
	private UserRepository repository;

	@MockBean
	private UserService service;

	private ModelMapper modelMapper;

	@Test
	public void saveUserTest(){

		UserDto userDto = new UserDto();
		userDto.setAge(20);
		userDto.setName("Md. Samim Hossain");
		userDto.setSex("Male");

		AddressDto addressDto = new AddressDto();
		addressDto.setAddressType("HOME");
		addressDto.setCity("Dhaka");
		addressDto.setHouse("Savar");
		addressDto.setZip(1344);
		userDto.setAddress(addressDto);

		User user = new User();
		user.setAge(20);
		user.setName("Md. Samim Hossain");
		user.setSex("Male");

		Address address = new Address();
		address.setAddressType("HOME");
		address.setCity("Dhaka");
		address.setHouse("Savar");
		address.setZip(1344);
		user.setAddress(address);

		Mockito.when(modelMapper.map(userDto,User.class))
				.thenReturn(user);

		Mockito.when(repository.save(user))
				.thenReturn(user);

		Mockito.when(modelMapper.map(user,UserDto.class))
				.thenReturn(userDto);

		UserDto resultDto = service.create(userDto);
		assertEquals(resultDto,userDto);


	}

}
