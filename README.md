# Simple Rest API using Spring Boot

This is an assignment project.Implemented code could me nice example for a rest API which mostly creates and search Users.Most interesting part of this code to update, delete and search a user by id and user can be create.


# Tech spec

<ul>
    <li>java 1.8</li>
    <li>spring boot 2.3</li>
    <li>maven</li>
    <li>junit 5</li>
</ul>

# Code structures (Modules/class Introduction)

<b>modules and packages inside main
<ul>
    <li>src/main/java/com/api/spring/entity - contain  entity class(Related to database table)</li>
    <li>src/main/java/com/api/spring/repository - contain repository interface,resposible to get data from database</li>
    <li>src/main/java/com/api/spring/rest - contain rest end-point and its model (dto)</li>
    <li>src/main/java/com/api/spring/custom/exception - contain exception related code</li>
    <li>src/main/java/com/api/spring/service - contain business logic</li>
    <li>src/main/resources - contain aplication properties</li>
</ul>

# How to run

<ul>
    <li>Server will run 8080 port by default</li><br>
    <li>API Documentation</li>
 </ul>
    
    http://localhost:8080/swagger-ui.html
    


# End points

<h4>create new user.</h4>
<ul>
    <li>id: Integer,primary key,auto generate</li>
    <li>name: String value</li>
    <li>age : Integr value,can not be null</li>
    <li> sex :String value</li>
    <li>address_id :Address type</li>
</ul>
User has oneToOne relation with address .Address table contain
 <ul>
    <li>id: Integer, primary key,auto generate</li>
    <li>addressType: String value, addressType can be only "Home" and "OFFICE"</li>
    <li>house: String value</li>
    <li>city: String value</li>
    <li>zip: any positive integer value</li>
 </ul>
 
```
 POST http://localhost:8080/api/users
content-Type: appliation/json

{
    "id": 4,
    "name": "samim",
    "age": 20,
    "sex": "male",
    "address": {
        "id": 6,
        "addressType": "HOME",
        "city": "Dhaka",
        "house": "Savar",
        "zip": 1344
    }
}
```


<h4>get user by id</h4>
<ul>
    <li>GET http://localhost:8080/api/users/ For example: </li>
</ul>

```GET http://localhost:8080/api/users/4```


<h4>update user</h4>
<ul>
    <li>PUT http://localhost:8080/api/users For example:
</ul>

```
PUT http://localhost:8080/api/users/4
content-type: application/json

{
    "id": 4,
    "name": "Md. Samim Hossain",
    "age": 20,
    "sex": "male",
    "address": {
        "id": 6,
        "addressType": "OFFICE",
        "city": "Dhaka",
        "house": "Savar",
        "zip": 1344
    }
}
```
<h4>delete user by id</h4>
<ul>
    <li>DELETE http://localhost:8080/api/users/ For example:</li>
</ul>

```
DELETE http://localhost:8080/api/users/4
```
 
 

